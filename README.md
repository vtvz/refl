Required permissions:

- `app_mentions:read`
- `chat:write`
- `chat:write.public`
- `im:write`
- `users:read`
- `users:read.email`

Example config:

```
default:
  accessToken: "Your Access Token"
  host: https://gitlab.com/
  mainBranch: master

projects:
  projectName:
    channelId: G12CVRD9JP2
    mainBranch: dev
    repos:
      - project/front
      - project/back

```
