module.exports = {
  candidateSuccess: (channel, ts, user, version, mergeRequests) => {
    return {
      channel: channel,
      ts: ts,
      text: `:white_check_mark: Branches and merge requests were created by <@${user}>`,
      blocks: [
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': `:white_check_mark: Branches and merge requests for version *${version}* were created by <@${user}>`,
          },
        },
        ...mergeRequests.map(([repo, url]) => (
            {
              'type': 'section',
              'text': {
                'type': 'mrkdwn',
                'text': `<${url}|${repo}>`,
              },
              'accessory': {
                'type': 'button',
                'text': {
                  'type': 'plain_text',
                  'text': 'Open',
                  'emoji': true,
                },
                'value': 'open',
                'url': url,
                'action_id': 'open',
              },
            }
          ),
        ),
      ],
    };
  },
  releaseSuccess: (channel, ts, user, version, tags) => {
    return {
      channel: channel,
      ts: ts,
      text: `:white_check_mark: Tags were created by <@${user}>`,
      blocks: [
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': `:white_check_mark: Tags for version *${version}* were created by <@${user}>`,
          },
        },
        ...tags.map(([repo, url, pipeline]) => (
            {
              'type': 'section',
              'text': {
                'type': 'mrkdwn',
                'text': `<${url}|${repo}>`,
              },
              ...(
                pipeline
                  ? {
                    'accessory': {
                      'type': 'button',
                      'text': {
                        'type': 'plain_text',
                        'text': 'Open pipeline',
                        'emoji': true,
                      },
                      'value': 'url',
                      'url': pipeline,
                      'action_id': 'url',
                    },
                  }
                  : {
                    'accessory': {
                      'type': 'button',
                      'text': {
                        'type': 'plain_text',
                        'text': 'Pipeline is not available',
                        'emoji': true,
                      },
                      'value': 'url',
                      'action_id': 'url',
                    },
                  }
              ),
            }
          ),
        ),
      ],
    };
  },
  welcome: () => {
    return {
      response_type: 'in_channel',
      blocks: [
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': ':fox_face: Select your action:',
          },
        },
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': '- Create *Release Candidate*',
          },
          'accessory': {
            'type': 'button',
            'text': {
              'type': 'plain_text',
              'text': 'Create',
              'emoji': true,
            },
            'value': 'candidate',
            'action_id': 'open_modal',
          },
        },
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': '- Create *Release*',
          },
          'accessory': {
            'type': 'button',
            'text': {
              'type': 'plain_text',
              'text': 'Create',
              'emoji': true,
            },
            'value': 'release',
            'action_id': 'open_modal',
          },
        },
      ],
    };
  },
  release: (trigger_id, channel, versions) => {
    return {
      trigger_id: trigger_id,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: 'Publish new release tag',
        },
        callback_id: 'publish_release',
        submit: {
          type: 'plain_text',
          text: 'Publish',
        },
        blocks: [
          {
            block_id: 'notes_block',
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Release notes',
            },
            element: {
              action_id: 'notes',
              type: 'plain_text_input',
              multiline: true,
              placeholder: {
                type: 'plain_text',
                text: 'Write release notes here',
              },
            },
          },
          {
            block_id: 'version_block',
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Version',
            },
            element: {
              action_id: 'version',
              type: 'static_select',
              placeholder: {
                type: 'plain_text',
                text: 'Select version to publish (Choose wisely)',
              },
              options: versions.map((version) => (
                {
                  text: {
                    type: 'plain_text',
                    text: version,
                  },
                  value: version,
                }
              )),
            },
          },
          {
            block_id: 'channel_block',
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Channel',
            },
            hint: {
              type: 'plain_text',
              text: 'Do not change it!',
            },
            element: {
              action_id: 'channel',
              type: 'plain_text_input',
              initial_value: channel,
              min_length: channel.length,
              max_length: channel.length,
            },
          },
        ],
      },
    };
  },
  candidate: (trigger_id, channel, mainBranch, versions) => {
    return {
      trigger_id: trigger_id,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: 'Publish new RC branch',
        },
        callback_id: 'publish_candidate',
        submit: {
          type: 'plain_text',
          text: 'Publish',
        },
        blocks: [
          {
            block_id: 'version_block',
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Version',
            },
            hint: {
              type: 'plain_text',
              text: `Branches will be created from ${mainBranch}`,
            },
            element: {
              action_id: 'version',
              type: 'static_select',
              placeholder: {
                type: 'plain_text',
                text: 'Select version to publish (Choose wisely)',
              },
              options: versions.map(([value, text]) => (
                {
                  text: {
                    type: 'plain_text',
                    text: text || value,
                  },
                  value: value,
                }
              )),
            },
          },
          {
            block_id: 'channel_block',
            type: 'input',
            label: {
              type: 'plain_text',
              text: 'Channel',
            },
            hint: {
              type: 'plain_text',
              text: 'Do not change it!',
            },
            element: {
              action_id: 'channel',
              type: 'plain_text_input',
              initial_value: channel,
              min_length: channel.length,
              max_length: channel.length,
            },
          },
        ],
      },
    };
  },
};
