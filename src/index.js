/* ********************************************************
 * Slack Node+Express Slash Commands Example with BlockKit
 *
 * Tomomi Imura (@girlie_mac)
 * ********************************************************/

const express = require('express');
const bodyParser = require('body-parser');
const SignatureVerifier = require('./SignatureVerifier');
const payloads = require('./payloads');
const { WebClient } = require('@slack/web-api');
const ConfigResolver = require('./ConfigResolver');
const ModalRelease = require('./Action/ModalRelease');
const ModalCandidate = require('./Action/ModalCandidate');
const PublishCandidate = require('./Action/PublishCandidate');
const PublishRelease = require('./Action/PublishRelease');

require('dotenv').config();

const configResolver = new ConfigResolver(process.env.CONFIG_PATH);
const slack = new WebClient(process.env.SLACK_TOKEN);
const signatureVerifier = new SignatureVerifier(process.env.SLACK_SIGNING_SECRET);

const app = express().disable('x-powered-by');

const rawBodyBuffer = (req, res, buf, encoding) => {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};

app.use(bodyParser.urlencoded({ verify: rawBodyBuffer, extended: true }));
app.use(bodyParser.json({ verify: rawBodyBuffer }));

const server = app.listen(process.env.PORT || 5000, () => {
  console.log('Express server listening on port %d in %s mode', server.address().port, app.settings.env);
});

app.post('/command', async (req, res) => {
  if (!signatureVerifier.verify(req)) {
    res.sendStatus(404);
    return;
  }

  const channelId = req.body.channel_id;

  require('yargs')
    .exitProcess(false)
    .help()
    .command('$0', 'Send message with actions', () => {}, (argv) => {
      if (configResolver.resolve(channelId)) {
        res.json(payloads.welcome());
        return;
      }

      res.json({
        response_type: 'in_channel',
        blocks: [
          {
            'type': 'section',
            'text': {
              'type': 'mrkdwn',
              'text': 'Project is not configured. Channel ID is `' + channelId + '`',
            },
          },
        ],
      });
    })
    .parse(req.body.text,
      (err, argv, output) => {
        if (argv.help) {
          res.json({
            response_type: 'in_channel',
            blocks: [
              {
                'type': 'section',
                'text': {
                  'type': 'mrkdwn',
                  'text': '```' + output + '```',
                },
              },
            ],
          });
        }
      },
    );
});

app.post('/interactive', async (req, res) => {
  // Verify the signing secret
  if (!signatureVerifier.verify(req)) {
    return res.status(404).send();
  }

  res.send('');
  const payload = JSON.parse(req.body.payload);

  switch (payload.type) {
    case 'block_actions':
      const action = payload.actions[0];

      switch (action.action_id) {
        case 'open_modal':
          switch (action.value) {
            case 'release':
              await new ModalRelease(configResolver, slack).handle(payload);
              break;
            case 'candidate':
              await new ModalCandidate(configResolver, slack).handle(payload);
              break;
          }
          break;
      }

      break;

    case 'view_submission':
      switch (payload.view.callback_id) {
        case 'publish_release':
          await new PublishRelease(configResolver, slack).handle(payload);
          break;

        case 'publish_candidate':
          await new PublishCandidate(configResolver, slack).handle(payload);
          break;
      }
      break;
  }
});
