const { Branches, MergeRequests, Tags, Pipelines } = require('@gitbeaker/node');

module.exports = class Api {
  /** @type {Branches} */
  branches;
  /** @type {Tags} */
  tags;
  /** @type {Pipelines} */
  pipelines;
  /** @type {MergeRequests} */
  mergeRequest;

  constructor(channelConfig) {
    const creds = {
      host: channelConfig.host,
      token: channelConfig.accessToken,
    };

    this.branches = new Branches(creds);
    this.tags = new Tags(creds);
    this.pipelines = new Pipelines(creds);
    this.mergeRequest = new MergeRequests(creds);
  }
};
