const fs = require('fs');
const YAML = require('yaml');
const _ = require('lodash');
const Api = require('./Api');

class ChannelConfig {
  /** @type string */
  channelId;
  /** @type string */
  accessToken;
  /** @type string */
  host;
  /** @type string */
  mainBranch;
  /** @type string[] */
  repos;

  api;

  constructor(config) {
    Object.assign(this, config);

    this.api = new Api(this);
  }
}

module.exports = class ConfigResolver {
  raw;
  /** @type {ChannelConfig[]} */
  config;

  constructor(path) { // process.env.CONFIG_PATH
    const file = fs.readFileSync(path, 'utf8');
    this.raw = YAML.parse(file);

    this._calculate();
  }

  _calculate() {
    const defaults = this.raw.default || {};
    this.config = _.mapValues(this.raw.projects, (value) => new ChannelConfig({ ...defaults, ...value }));
  }

  /**
   * @param channelId
   * @returns {ChannelConfig}
   */
  resolve(channelId) {
    return _.find(this.config, (config) => config.channelId === channelId);
  }
};
