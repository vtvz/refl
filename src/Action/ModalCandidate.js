const payloads = require('../payloads');
const { WebClient } = require('@slack/web-api');

module.exports = class ModalRelease {
  configResolver;
  slack;

  /**
   *
   * @param configResolver {ConfigResolver}
   * @param slack {WebClient}
   */
  constructor(configResolver, slack) {
    this.configResolver = configResolver;
    this.slack = slack;
  }

  async getBranchesVersionMap(api, repos) {
    let majors = new Set();
    const minors = {};

    const branches = await api.all(repos[0], { search: '^rc-', perPage: 100 });

    for (const branch of branches) {
      const [_, major, minor] = /^rc-([\d]+)\.([\d]+)\.x$/.exec(branch.name);
      majors.add(parseInt(major));
      minors[parseInt(major)] = [parseInt(minor), ...(minors[major] || [])];
    }

    majors = [...majors];

    return { majors, minors };
  }

  async handle(payload) {
    const channel = payload.channel.id;
    const config = this.configResolver.resolve(channel);

    const { majors, minors } = await this.getBranchesVersionMap(config.api.branches, config.repos);

    const versions = [];

    for (const major of majors) {
      const version = `${major}.${Math.max(...minors[major]) + 1}.x`;
      versions.push([version]);
    }

    if (0 === majors.length) {
      versions.push(['0.1.x', '0.1.x (major)']);
    } else {
      const version = `${Math.max(...majors) + 1}.0.x`;
      versions.push([version, `${version} (major)`]);
    }
    await this.slack.views.open(payloads.candidate(payload.trigger_id, channel, config.mainBranch, versions.sort().reverse()));
  }
};
