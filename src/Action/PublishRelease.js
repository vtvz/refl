const payloads = require('../payloads');
const { WebClient } = require('@slack/web-api');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = class SubmitCandidate {
  configResolver;
  slack;

  /**
   *
   * @param configResolver {ConfigResolver}
   * @param slack {WebClient}
   */
  constructor(configResolver, slack) {
    this.configResolver = configResolver;
    this.slack = slack;
  }

  async handle(payload) {
    const version = payload.view.state.values.version_block.version.selected_option.value;
    const channel = payload.view.state.values.channel_block.channel.value;
    const notes = payload.view.state.values.notes_block.notes.value;
    const user = payload.user.id;

    const tagName = 'v' + version;
    const [_, minor] = /^v([\d]+\.[\d]+)\.([\d]+)$/.exec(tagName);
    const branch = `rc-${minor}.x`;

    const config = this.configResolver.resolve(channel);

    const message = await this.slack.chat.postMessage({
      channel: channel,
      text: 'Release in progress :hourglass_flowing_sand:',
    });

    const tags = [];
    for (const repo of config.repos) {
      await config.api.tags.create(repo, {
        tag_name: tagName,
        ref: branch,
        release_description: notes,
      });

      await sleep(1000);

      const pipelines = await config.api.pipelines.all(repo, { perPage: 1, ref: tagName });
      let pipeline = undefined;
      if (pipelines[0]) {
        pipeline = pipelines[0].web_url;
      }

      tags.push([repo, `${config.host}/${repo}/-/tags/${tagName}`, pipeline]);
    }

    await this.slack.chat.update(payloads.releaseSuccess(message.channel, message.ts, user, version, tags));
  }
};
