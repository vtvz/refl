const payloads = require('../payloads');
const compareVersions = require('compare-versions');
const { WebClient } = require('@slack/web-api');

module.exports = class ModalRelease {
  configResolver;
  slack;

  /**
   *
   * @param configResolver {ConfigResolver}
   * @param slack {WebClient}
   */
  constructor(configResolver, slack) {
    this.configResolver = configResolver;
    this.slack = slack;
  }

  async handle(payload) {
    const channel = payload.channel.id;
    const config = this.configResolver.resolve(channel);

    const branches = await config.api.branches.all(config.repos[0], { search: '^rc-', perPage: 100 });

    if (0 === branches.length) {
      await this.slack.chat.postMessage({
        channel: channel,
        text: 'There are no Release Candidates in project :pensive:',
      });
      return;
    }

    const rcVersions = {};
    for (const branch of branches) {
      const [_, version] = /^rc-([\d]+\.[\d]+)\.x$/.exec(branch.name);
      rcVersions[version] = [];
    }

    const tags = await config.api.tags.all(config.repos[0], { search: '^v', perPage: 100 });

    for (const tag of tags) {
      const [_, version, patch] = /^v([\d]+\.[\d]+)\.([\d]+)$/.exec(tag.name);
      rcVersions[version].push(parseInt(patch));
    }

    const versions = [];
    for (const version in rcVersions) {
      const patches = rcVersions[version];

      if (0 === patches.length) {
        versions.push(version + '.0');
      } else {
        versions.push(version + '.' + (Math.max(...patches) + 1));
      }
    }

    await this.slack.views.open(payloads.release(payload.trigger_id, channel, versions.sort(compareVersions).reverse()));
  }
};
