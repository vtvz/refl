const payloads = require('../payloads');
const { WebClient } = require('@slack/web-api');

module.exports = class SubmitRelease {
  configResolver;
  slack;

  /**
   *
   * @param configResolver {ConfigResolver}
   * @param slack {WebClient}
   */
  constructor(configResolver, slack) {
    this.configResolver = configResolver;
    this.slack = slack;
  }

  async handle(payload) {
    const version = payload.view.state.values.version_block.version.selected_option.value;
    const channel = payload.view.state.values.channel_block.channel.value;
    const user = payload.user.id;

    const branchName = `rc-${version}`;

    const config = this.configResolver.resolve(channel);

    const message = await this.slack.chat.postMessage({
      channel: channel,
      text: 'Release Candidate creation in progress :hourglass_flowing_sand:',
    });

    const mergeRequests = [];
    for (const repo of config.repos) {
      await config.api.branches.create(repo, branchName, config.mainBranch);
      const mergeRequest = await config.api.mergeRequest.create(repo, branchName, config.mainBranch, `Release candidate ${version}`);

      mergeRequests.push([repo, mergeRequest.web_url]);
    }

    await this.slack.chat.update(payloads.candidateSuccess(message.channel, message.ts, user, version, mergeRequests));
  }
};
