FROM node:alpine

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --frozen-lockfile --production && yarn cache clean

COPY src ./src

ENV PORT 80
EXPOSE 80

CMD [ "yarn", "start" ]
